import sys

if sys.version_info.major == 2:
    from platforms.python2 import *
else:
    from platforms.python3 import *
