#!/usr/bin/awk -f
BEGIN{ FS = "[: )\t/(]+" }
/events\/s \(eps\):/ { printf"Throughput_eps: %.2f\n",$NF }
/transferred \(.* MiB\/sec\)/ { printf"Throughput_MB: %.2f MB/s\n",$(NF-3) }
/total number of events:/ { printf"workload: %.2f\n",$NF }
/min:/ { printf"latency_min: %.2f ms\n",$NF }
/avg:/ { printf"latency_avg: %.2f ms\n",$NF }
/max:/ { printf"latency_max: %.2f ms\n",$NF }
/95th percentile:/ { printf"latency_95th: %.2f ms\n",$NF }
/events \(avg\/stddev\):/ { 
	printf"thread_events_avg: %.2f\nthread_events_stddev: %.2f\n",$(NF-1),$NF
}
/execution time \(avg\/stddev\):/ { 
	printf"exec_time_avg: %.4f s\nexec_time_stddev: %.2f\n",$(NF-1),$NF
}

