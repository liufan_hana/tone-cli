#!/bin/bash
# Avaliable environment variables:
# $PACKAGE_NAME: package name, specify one or more packages to test, seperated by space ' '.
# $REPO_ID: repo id, specify one or more repos to test, seperated by space ' '.
# $CHECK_INSTALLED: test installed pkg flag, test installed pkg of current system when the value is "y".
. $TONE_ROOT/lib/testinfo.sh
. $TONE_BM_SUITE_DIR/pkg_test_lib.sh

setup(){
    set_coredump
}

run() {
    
    [ -n "$group" ] || {
        echo "No group in specfile"
        return 1
    }
    if [ "$group" == "cmd_test" ]; then
        cmd_run
    elif [ "$group" == "install_test" ]; then
        install_run
    elif [ "$group" == "service_test" ]; then
        service_run
    elif [ "$group" == "pkg_update_test" ]; then
        pkg_update_run
    else
        echo "$group is not supported" 
    fi 
}

teardown() {
    clean_coredump
}

parse() {

    if [ "$group" == "cmd_test" ]; then
        cat $RESULT_PATH/pkg_help_test_result.log
    elif [ "$group" == "install_test" ]; then
        #cat $RESULT_PATH/pkg_install_and_uninstall_test_result_by_repo.log
        cat $RESULT_PATH/pkg_install_and_uninstall_test_result_by_pkg.log
    elif [ "$group" == "service_test" ]; then
        cat $RESULT_PATH/pkg_service_check_test_result.log
    elif [ "$group" == "pkg_update_test" ]; then
        cat $RESULT_PATH/pkg_update_test_result.log
    else
        return 1 
    fi 

}
pkg_update_run()
{
    ls $RESULT_PATH || mkdir -p $RESULT_PATH
	yum clean all ||{
    echo "pkg_update_test: FAIL" > $RESULT_PATH/pkg_update_test_result.log
    return 1
    }
    yum makecache ||{
    echo "pkg_update_test: FAIL" > $RESULT_PATH/pkg_update_test_result.log
    return 1
    }
    yum update -y ||{
    echo "pkg_update_test: FAIL" > $RESULT_PATH/pkg_update_test_result.log
    return 1
    }   
    echo "pkg_update_test: PASS" > $RESULT_PATH/pkg_update_test_result.log
}
service_run()
{
    local loop
    rm -rf $RESULT_PATH
    mkdir -p $RESULT_PATH
    if [ -n "$PACKAGE_NAME" ]; then
        for loop in ${PACKAGE_NAME[@]}; do
            service_check $loop 
        done
    elif [ -n "$REPO_ID" ]; then
        for loop in ${REPO_ID[@]}; do
            service_check_by_repo $loop 
        done
    else
        yum repolist | awk '{if (NR>1){print $1}}' >$RESULT_PATH/repolist.txt
        while read line; do
            service_check_by_repo  $line
        done <$RESULT_PATH/repolist.txt
    fi
    cp -rH $RESULT_PATH $TONE_CURRENT_RESULT_DIR
    cp -rH $COREDUMP_PATH $TONE_CURRENT_RESULT_DIR

}
cmd_run()
{
    local loop
    rm -rf $RESULT_PATH
    mkdir -p $RESULT_PATH
    if [ -n "$PACKAGE_NAME" ]; then
        for loop in ${PACKAGE_NAME[@]}; do
            exec_by_pkg $loop check_coredump
        done
    elif [ -n "$REPO_ID" ]; then
        for loop in ${REPO_ID[@]}; do
            exec_by_repo $loop check_coredump
        done
    elif [ "$CHECK_INSTALLED" == "y" ]; then
        gen_installed_list
        while read line; do
            exec_by_pkg $line check_coredump
        done <$RESULT_PATH/installed.packages.list
    else
        get_repolist
    fi

    cp -rH $RESULT_PATH $TONE_CURRENT_RESULT_DIR
    cp -rH $COREDUMP_PATH $TONE_CURRENT_RESULT_DIR
}
install_run()
{
    local loop
    rm -rf $RESULT_PATH
    mkdir -p $RESULT_PATH
    if [ -n "$PACKAGE_NAME" ]; then
        for loop in ${PACKAGE_NAME[@]}; do
            inst_and_uninst_by_pkg $loop
        done
    elif [ -n "$REPO_ID" ]; then
        for loop in ${REPO_ID[@]}; do
            inst_and_uninst_by_repo $loop
        done
    else
        yum repolist | awk '{if (NR>1){print $1}}' >$RESULT_PATH/repolist.txt
        while read line; do
            inst_and_uninst_by_repo $line
        done <$RESULT_PATH/repolist.txt
    fi

    cp -rH $RESULT_PATH $TONE_CURRENT_RESULT_DIR
    cp -rH $COREDUMP_PATH $TONE_CURRENT_RESULT_DIR
}
