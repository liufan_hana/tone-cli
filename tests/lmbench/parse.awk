#!/usr/bin/awk -f
BEGIN { FS="[ |=|:|\t]+" }

function lastone() {
    while (getline > 0 && NF)
        bandwidth_data = bandwidth_data" "$0
    n=split(bandwidth_data,bandwidth)
    return bandwidth[n]
}

#benchmark_file
/^"File system latency$/ {
    while (getline > 0 && NF)
        if ($1 ~ /^0k/) {
            printf("0k_file_create: %.4f \n",1000/$3*1000)
            printf("0k_file_delete: %.4f \n",1000/$4*1000)
        }	
        else if ($1 ~ /^10k/) {
            printf("10k_file_create: %.4f \n",10*1000/$3*1000)
            printf("10k_file_delete: %.4f \n",10*1000/$4*1000)
        }	
}


/^"read bandwidth$/ {
    read_bandwidth = lastone()
    printf("read_bandwidth: %.2f MB/s\n",read_bandwidth)
}

/^"read open2close bandwidth$/ {
    open2close_bandwidth = lastone()
    printf("open2close_bandwidth: %.2f MB/s\n",open2close_bandwidth)
}

#benchmark_select
/Select on [0-9]+ fd's:/ { printf"Select_%dfd_latency: %.4f us\n",$3,$5 }
/Select on [0-9]+ tcp fd's:/ { printf"Select_%dtcp_latency: %.4f us\n",$3,$6 }

#benchmark_proc
/Process fork\+exit:/ { printf"Process_fork+exit: %.4f us\n",$3 }
/Process fork\+execve:/ { printf"Process_fork+execve: %.4f us\n",$3 }
/Process fork\+\/bin\/sh/ { printf"Process_fork+/bin/sh-c: %.4f us\n",$4 }

#benchmark_pagefault
/Pagefaults on .*:/ { printf"Pagefaults: %.4f us\n",$4 }

#benchmark_bcopy
/^"libc bcopy unaligned$/ {
    libc_bcopy_unaligned = lastone()
    printf("libc_bcopy_unaligned: %.2f MB/s\n",libc_bcopy_unaligned)
}

/^"unrolled bcopy unaligned$/ {
    unrolled_bcopy_unaligned = lastone()
    printf("unrolled_bcopy_unaligned: %.2f MB/s\n",unrolled_bcopy_unaligned)
}

/^Memory read bandwidth$/ {
    memory_read_bandwidth = lastone()
    printf("memory_read_bandwidth: %.2f MB/s\n",memory_read_bandwidth)
}

/^Memory write bandwidth$/ {
    memory_write_bandwidth = lastone()
    printf("memory_write_bandwidth: %.2f MB/s\n",memory_write_bandwidth)
}

#benchmark_pipe
/^Pipe latency:/ { printf"pipe_latency: %.4f us\n",$3 }
/^Pipe bandwidth:/ { printf"pipe_bandwidth: %.4f MB/s\n",$3}

#benchmark_mmap
/^"Mmap read bandwidth$/ {
    mmap_read_bandwidth = lastone()
    printf("mmap_read_bandwidth: %.2f MB/s\n",mmap_read_bandwidth)
}

/^"Mmap read open2close bandwidth$/ {
    mmap_read_open2close_bandwidth = lastone()
    printf("mmap_read_open2close_bandwidth: %.2f MB/s\n",mmap_read_open2close_bandwidth)
}

#benchmark_ops
/^(integer|int64|float|double) (bit|add|mul|div|mil): [0-9.]+ nanoseconds$/ {
    printf("%s_%s_latency: %.2f ns\n",$1,$2,$3)
}

#benchmark_syscall
/^Simple .*: [0-9.]+ microseconds$/ {
    printf("%s_%s_latency: %.2f us\n",$1,$2,$3)
}

#benchmark_ctx
/^"size=(64|16|0)k ovr=[0-9.]+/ {
    size = $2
    #result = lastone()
    while (getline > 0 && NF)
        if ($1 == 2 ) {
            printf("2p_%s_latency: %.2f us\n",size,$2)
        }
        else if ($1 == 8) {
            printf("8p_%s_latency: %.2f us\n",size,$2)	
        }
        else if ($1 == 16) {
            printf("16p_%s_latency: %.2f us\n",size,$2)	
        }
}

#benchmark_unix
/^AF_UNIX sock stream latency: [0-9.]+ microseconds$/ {
    printf("%s_%s_%s_%s: %.4f us\n",$1,$2,$3,$4,$5)
}

/^AF_UNIX sock stream bandwidth:/ {
    printf("%s_%s_%s_%s: %.4f MB/s\n",$1,$2,$3,$4,$5)
}


#benchmark_udp/tcp
/^[UT][DC]P latency using localhost: [0-9.]+ microseconds$/ {
    printf("%s_%s_%s: %.4f us\n",$1,$2,$4,$5)
}

/^Socket bandwidth using localhost$/ {
    while (getline > 0 && NF)
        printf("Socket_bw_localhost_%sMB: %.2f MB/s\n",$1,$2)
}

#benchmark_connect
/^TCP\/IP connection cost to localhost: [0-9.]+ microseconds$/ {
    printf("%s_%s_%s: %.4f us\n",$1,$2,$5,$6)
}

#benchmark_http
/^Avg xfer: [0-9.]+KB, [0-9.]+KB in [0-9.]+ millisecs, [0-9.]+ MB\/sec$/ {
    printf("avg_xfer_url_size: %.2f KB\n",$3)
    printf("avg_xfer_workload: %.2f KB\n",$4)
    printf("avg_xfer_time: %.4f ms\n",$6)
    printf("avg_xfer_throughput: %.2f MB/s\n",$(NF-1))
}

#benchmark_rpc
/^RPC\/[ut][dc]p latency using localhost: [0-9.]+ microseconds$/ {
    printf("%s_%s_%s: %.4f us\n",$1,$2,$4,$5)
}

#benchmark_mem
/^Memory load latency$/ {
    getline
    stride="stride"$NF
    result = lastone()
    printf("main_mem_%s_latency: %.4f \n",stride,result)
}

/^Random load latency$/ {
    getline
    stride="stride"$NF
    result = lastone()
    printf("rand_mem_%s_latency: %.4f \n",stride,result)
}
