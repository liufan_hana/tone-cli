#!/bin/bash

compile_image="localhost/compile-image:latest"
nydus_snapshotter_repo="https://github.com/containerd/nydus-snapshotter.git"
nydus_image="docker.io/hsiangkao/ubuntu:20.04-rafs-v6"
result_summary_file="$TONE_BM_RUN_DIR/result_summary.log"

git_merge_target_branch()
{
	git status
	if [ -n "$PKG_CI_REPO_TARGET_BRANCH" ] && [ -n "$PKG_CI_REPO_TARGET_URL" ]; then
		echo "PKG_CI_REPO_TARGET_URL is $PKG_CI_REPO_TARGET_URL"
		echo "PKG_CI_REPO_TARGET_BRANCH is $PKG_CI_REPO_TARGET_BRANCH"
		git pull --no-edit $PKG_CI_REPO_TARGET_URL $PKG_CI_REPO_TARGET_BRANCH
		return $?
	fi
}

build_rust_golang_image()
{
	yum install -y docker
	if [ $? -ne 0 ]; then
		echo "fail to install docker"
		return 1
	fi

	cat > $TONE_BM_RUN_DIR/rust_golang_dockerfile <<EOF
FROM rust:1.61.0

RUN apt-get update -y \
    && apt-get install -y cmake g++ pkg-config jq libcurl4-openssl-dev libelf-dev libdw-dev binutils-dev libiberty-dev musl-tools \
    && rustup component add rustfmt clippy \
    && rm -rf /var/lib/apt/lists/*

# install golang env
Run wget https://go.dev/dl/go1.18.7.linux-amd64.tar.gz \
    && tar -C /usr/local -xzf go1.18.7.linux-amd64.tar.gz \
    && rm -rf go1.18.7.linux-amd64.tar.gz

ENV PATH \$PATH:/usr/local/go/bin
RUN go env -w GO111MODULE=on
RUN go env -w GOPROXY=https://goproxy.io,direct
EOF
	docker build -f $TONE_BM_RUN_DIR/rust_golang_dockerfile -t $compile_image .
	return $?
}

compile_nydus()
{
	docker run --rm -v `pwd`:/image-service $compile_image bash -c 'cd /image-service && make clean && make release'
	if [ $? -ne 0 ]; then
		return 1
	else
		if [ -f "target/release/nydusd" ] && [ -f "target/release/nydus-image" ]; then
			/usr/bin/cp -f target/release/nydusd /usr/local/bin/
			/usr/bin/cp -f target/release/nydus-image /usr/local/bin/
			return 0
		else
			echo "cannot find nydusd binary or nydus-image binary"
			return 1
		fi
	fi
}

compile_ctr_remote()
{
	docker run --rm -v `pwd`:/image-service $compile_image bash -c 'cd /image-service/contrib/ctr-remote && make clean && make'
	if [ $? -ne 0 ]; then
		return 1
	else
		if [ -f "contrib/ctr-remote/bin/ctr-remote" ]; then
			/usr/bin/cp -f contrib/ctr-remote/bin/ctr-remote /usr/local/bin/
			return 0
		else
			echo "cannot find ctr-remote binary"
			return 1
		fi
	fi
}

compile_nydus_snapshotter()
{
	rm -rf nydus-snapshotter
	git clone "$nydus_snapshotter_repo"
	if [ $? -ne 0 ]; then
		echo "fail to clone $nydus_snapshotter_repo"
		return 1
	else
		docker run --rm -v `pwd`/nydus-snapshotter:/nydus-snapshotter $compile_image bash -c 'cd /nydus-snapshotter && make clear && make'
		if [ $? -ne 0 ]; then
			return 1
		else
			if [ -f "nydus-snapshotter/bin/containerd-nydus-grpc" ]; then
				/usr/bin/cp -f nydus-snapshotter/bin/containerd-nydus-grpc /usr/local/bin/
				echo "nydus-snapshotter version"
				containerd-nydus-grpc --version
				return 0
			else
				echo "cannot find containerd-nydus-grpc binary"
				return 1
			fi
		fi
	fi
}

start_nydus_snapshotter_and_config_containerd()
{
	cat > $TONE_BM_RUN_DIR/nydus-erofs-config.json <<EOF
{
  "type": "bootstrap",
  "config": {
    "backend_type": "registry",
    "backend_config": {
      "scheme": "https"
    },
    "cache_type": "fscache"
  }
}
EOF
	rm -fr /var/lib/containerd/io.containerd.snapshotter.v1.nydus
	rm -fr /var/lib/nydus/cache
	containerd-nydus-grpc --config-path $TONE_BM_RUN_DIR/nydus-erofs-config.json --daemon-mode shared \
		--daemon-backend fscache --log-level info --root /var/lib/containerd/io.containerd.snapshotter.v1.nydus \
		--cache-dir /var/lib/nydus/cache --address /run/containerd/containerd-nydus-grpc.sock \
		--nydusd-path /usr/local/bin/nydusd --log-to-stdout > $TONE_BM_RUN_DIR/nydus-snapshotter.log 2>&1 &

	# config containerd for nydus
	[ -d "/etc/containerd" ] || mkdir -p /etc/containerd
	cat > /etc/containerd/config.toml <<EOF
version = 2

[plugins]
  [plugins."io.containerd.grpc.v1.cri"]
    [plugins."io.containerd.grpc.v1.cri".cni]
      bin_dir = "/usr/lib/cni"
      conf_dir = "/etc/cni/net.d"
  [plugins."io.containerd.internal.v1.opt"]
    path = "/var/lib/containerd/opt"

[proxy_plugins]
  [proxy_plugins.nydus]
    type = "snapshot"
    address = "/run/containerd/containerd-nydus-grpc.sock"

[plugins."io.containerd.grpc.v1.cri".containerd]
   snapshotter = "nydus"
   disable_snapshot_annotations = false
EOF
	systemctl restart containerd
	return $?
}

run_container_with_nydus_image()
{
	ctr-remote images rpull $nydus_image
	if [ $? -ne 0 ]; then
		echo "fail to pull $nydus_image with ctr-remote"
		return 1
	else
		ctr run --rm --snapshotter=nydus $nydus_image ubuntu tar cvf /tmp/foo.tar --exclude=/sys --exclude=/proc --exclude=/dev /
		return $?
	fi
}

run()
{
	rm -f $result_summary_file
	# git merge target branch
	echo -e "======== git merge target branch"
	git_merge_target_branch
	if [ $? -eq 0 ]; then
		echo "merge-target-branch: pass" | tee -a $result_summary_file
		git status
	else
		echo "merge-target-branch: fail" | tee -a $result_summary_file
		return
	fi

	# build docker image for compile
	echo -e "\n======== build rust and golang image for compile"
	build_rust_golang_image
	if [ $? -eq 0 ]; then
		echo "build-docker-image: pass" | tee -a $result_summary_file
		docker images
	else
		echo "build-docker-image: fail" | tee -a $result_summary_file
		return
	fi

	# compile nydusd and nydus-image
	echo -e "\n======== compile nydusd and nydus-image"
	compile_nydus
	if [ $? -eq 0 ]; then
		echo "compile-nydus: pass" | tee -a $result_summary_file
	else
		echo "compile-nydus: fail" | tee -a $result_summary_file
		return
	fi

	# compile ctr-remote
	echo -e "\n======== compile ctr-remote"
	compile_ctr_remote
	if [ $? -eq 0 ]; then
		echo "compile-ctr-remote: pass" | tee -a $result_summary_file
	else
		echo "compile-ctr-remote: fail" | tee -a $result_summary_file
		return
	fi

	# compile nydus-snapshotter
	echo -e "\n======== compile nydus-snapshotter"
	compile_nydus_snapshotter
	if [ $? -eq 0 ]; then
		echo "compile-nydus-snapshotter: pass" | tee -a $result_summary_file
	else
		echo "compile-nydus-snapshotter: fail" | tee -a $result_summary_file
		return
	fi

	# start nydus-snapshotter
	echo -e "\n======== start nydus-snapshotter and config containerd for nydus"
	start_nydus_snapshotter_and_config_containerd
	if [ $? -eq 0 ]; then
		echo "start-nydus-snapshotter-config-containerd: pass" | tee -a $result_summary_file
	else
		echo "start-nydus-snapshotter-config-containerd: fail" | tee -a $result_summary_file
		return
	fi

	# run a container with nydus image
	sleep 3
	echo -e "\n======== run a container with nydus image"
	run_container_with_nydus_image
	if [ $? -eq 0 ]; then
		echo "run-container-with-nydus-image: pass" | tee -a $result_summary_file
	else
		echo "run-container-with-nydus-image: fail" | tee -a $result_summary_file
	fi
	dmesg -T | tail -300 > $TONE_BM_RUN_DIR/dmesg.log
}

teardown()
{
	[ -f "$TONE_BM_RUN_DIR/nydus-snapshotter.log" ] && /usr/bin/cp $TONE_BM_RUN_DIR/nydus-snapshotter.log $TONE_CURRENT_RESULT_DIR/
	[ -f "$TONE_BM_RUN_DIR/dmesg.log" ] && /usr/bin/cp $TONE_BM_RUN_DIR/dmesg.log $TONE_CURRENT_RESULT_DIR/
	
	# clean up nydus image
	if ctr images ls | grep $nydus_image; then
		echo "======== clean up nydus image"
		ctr images rm $nydus_image
		if [ $? -ne 0 ]; then
			echo "fail to remove $nydus_image with ctr"
		else
			echo "succeed to remove $nydus_image with ctr"
		fi
	fi

	# kill containerd-nydus-grpc process
	if ps -ef | grep containerd-nydus-grpc | grep -v grep; then
		ps -ef | grep containerd-nydus-grpc | grep -v grep | awk '{print $2}' | xargs kill -9
	fi

	# kill nydusd process
	if ps -ef | grep "/usr/local/bin/nydusd" | grep -v grep; then
		ps -ef | grep "/usr/local/bin/nydusd" | grep -v grep | awk '{print $2}' | xargs kill -9
	fi

	if mount | grep 'erofs on'; then
		mount | grep 'erofs on' | awk '{print $3}' | xargs umount
	fi
}

parse()
{
	cat $TONE_BM_RUN_DIR/result_summary.log
}
