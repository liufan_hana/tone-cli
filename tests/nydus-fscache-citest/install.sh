GIT_URL=${PKG_CI_REPO_SOURCE_URL:-"https://github.com/dragonflyoss/image-service.git"}
BRANCH=${PKG_CI_REPO_SOURCE_BRANCH:-"master"}

build()
{
	:
}

install()
{
	echo "PKG_CI_REPO_SOURCE_URL is $PKG_CI_REPO_SOURCE_URL"
	echo "PKG_CI_REPO_SOURCE_BRANCH is $PKG_CI_REPO_SOURCE_BRANCH"
	set_git_clone
	git_clone_exec $TONE_BM_CACHE_DIR $TONE_BM_RUN_DIR --branch "${BRANCH}"
}
