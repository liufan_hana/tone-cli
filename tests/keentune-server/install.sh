#!/bin/bash

TONE_CAN_SKIP_PKG="yes"
DEP_PKG_LIST="python36 python36-devel golang rust openssl-devel git kernel-headers"


function version_ge()
{
    test "$(echo "$@" | tr " " "\n" | sort -rV | head -n 1)" == "$1"; 
}

check_kernel_headers()
{
    rpm -qa|grep kernel-headers-$(uname -r) && return 0
    local version=$(yum list --showduplicates  kernel-headers-`uname -r` |tail -1|awk '{print $2}')
    [ -n "$version" ] && yum install -y kernel-headers-$version && return 0 || return 1
}

check_go_version()
{
    local cur_version=$(go version |awk '{print $3}')
    local min_version="1.15"
    if [ -n "$cur_version" ]; then
        version_ge "$cur_version" "$min_version" && return 0
        yum remove -y golang
    fi
    rpm -q epel-release || yum install -y epel-release
    yum install -y golang
    
    local new_version=$(go version |awk '{print $3}')
    version_ge "$new_version" "$min_version" && return 0 || return 1
}

set_go_env()
{
    check_go_version
    go env -w GO111MODULE=on
    go env -w GOPROXY=https://goproxy.cn,direct
    go env
    go version
}

check_rust()
{
    rpm -q rust && return 0
    rpm -q epel-release || yum install -y epel-release
    yum install rust -y && return 0 || return 1
}

set_env()
{
    check_kernel_headers
    set_go_env
    check_rust
}

build()
{
    set_env
}

install()
{
    echo "install server finshed......."
}
