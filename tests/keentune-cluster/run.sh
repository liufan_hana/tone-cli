#!/bin/bash
# Avaliable environment:
#
# Download variable:
# WEB_URL=
# GIT_URL=

. $TONE_ROOT/lib/testinfo.sh
. $TONE_ROOT/lib/common.sh

log_suffix="$code_source-$test_branch-$(date +"%Y-%m-%d-%H-%M-%S")"
keentune_code_dir=/tmp/keentune-cluster-tmp

[ -z "$SERVER" ] && echo "No Server IP provided,exit 1" >>$error_log && exit 1
[ -z "$CLIENT_1" ] && echo "No Client 1 IP provided,exit 1" >>$error_log && exit 1
[ -z "$CLIENT_2" ] && echo "No Client 2 IP provided,exit 1" >>$error_log && exit 1
[ -z "$CLIENT_3" ] && echo "No Client 3 IP provided,exit 1" >>$error_log && exit 1

ESSH="ssh -o StrictHostKeyChecking=no"
ESCP="scp -o StrictHostKeyChecking=no"

ESSH_CLIENT1="$ESSH root@$CLIENT_1"
ESSH_CLIENT2="$ESSH root@$CLIENT_2"
ESSH_CLIENT3="$ESSH root@$CLIENT_3"
ESCP_CLIENT1="$ESCP root@$CLIENT_1"
ESCP_CLIENT2="$ESCP root@$CLIENT_2"
ESCP_CLIENT3="$ESCP root@$CLIENT_3"

REMOTE_DIR=/tmp/

log_error(){
      echo -e "\033[31;1m"ERROR:$1"\033[0m"
}


# this function can be used when keentune is running
clear_keentune_job()
{
    echo y | keentune param delete --job param1
    echo y | keentune sensitize delete --job param1
    echo y | keentune param delete --job sensitize1
    echo y | keentune sensitize delete --job sensitize1
}

setup()
{
	setup_fail=0
    logger $ESSH_CLIENT1 "sh -x $TONE_BM_SUITE_DIR/build_keentune.sh gitee $keentune_branch CLIENT1"
	if [ $?  -ne 0 ];then
		log_error "build_keentune.sh fail on CLIENT1:$CLIENT_1"
		setup_fail=1
	fi

    logger $ESSH_CLIENT2 "sh -x $TONE_BM_SUITE_DIR/build_keentune.sh gitee $keentune_branch CLIENT2" 
	if [ $?  -ne 0 ];then
		log_error "build_keentune.sh fail on CLIENT2:$CLIENT_2"
		setup_fail=1
	fi

    logger $ESSH_CLIENT3 "sh -x $TONE_BM_SUITE_DIR/build_keentune.sh gitee $keentune_branch CLIENT3" 
	if [ $?  -ne 0 ];then
		log_error "build_keentune.sh fail on CLIENT3:$CLIENT_3"
		setup_fail=1
	fi
	
	logger sh -x ${TONE_BM_SUITE_DIR}/build_keentune.sh gitee $keentune_branch SERVER
	if [ $?  -ne 0 ];then
		log_error "build_keentune.sh fail on SERVER:$SERVER"
		setup_fail=1
	fi
	[ $setup_fail -eq 1 ] &&  log_error "setup fail" || return 0
}

run_fail()
{
    cp $TONE_BM_RUN_DIR/keentuned-$log_suffix $TONE_CURRENT_RESULT_DIR/
    cp $TONE_BM_RUN_DIR/keentune-brain-$log_suffix $TONE_CURRENT_RESULT_DIR/
    cp $TONE_BM_RUN_DIR/keentune-bench-$log_suffix $TONE_CURRENT_RESULT_DIR/
    cp $TONE_BM_RUN_DIR/keentune-target-$log_suffix $TONE_CURRENT_RESULT_DIR/
    exit 1
}

run()
{
	[ $setup_fail -eq 1 ] && return 1
	
    clear_keentune_job

    if [ "$test_branch" = "master" ]; then
        # logger cd $TONE_BM_RUN_DIR/acops-new/test
		logger cd $keentune_code_dir/acops-new/test
    elif [ "$test_branch" = "master-open" ]; then
        logger cd $TONE_BM_RUN_DIR/acops-new/Tests
    fi

    sleep 5
    echo -e "Start test:python3 main.py\nTests will take several minutes, pls wait"
    python3 main.py
    if [ $? -ne 0 ]; then
        cd $TONE_BM_RUN_DIR/
        echo "ERROR:python3 main.py failed"
        run_fail
    fi

    cd $keentune_code_dir/
}

parse()
{
    awk -f $TONE_BM_SUITE_DIR/parse.awk
}

teardown()
{
    $ESSH_CLIENT1 "sh -x $TONE_BM_SUITE_DIR/clear_remote.sh >$REMOTE_DIR/client1_install_log"
    $ESSH_CLIENT2 "sh -x $TONE_BM_SUITE_DIR/clear_remote.sh >$REMOTE_DIR/client2_install_log"
    $ESSH_CLIENT3 "sh -x $TONE_BM_SUITE_DIR/clear_remote.sh >$REMOTE_DIR/client3_install_log"
    sh ${TONE_BM_SUITE_DIR}/clear_remote.sh
}
