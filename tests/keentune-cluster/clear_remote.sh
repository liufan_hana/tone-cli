#!/bin/bash
keentune_tmp_dir=/tmp/keentune-cluster-tmp
nginx_conf=/etc/nginx/nginx.conf
ps -ef|grep -E 'keentuned|keentune-brain|keentune-target|keentune-bench'|grep -v grep|awk '{print $2}'| xargs -I {} kill -9 {}

[ -s "$keentune_tmp_dir/nr_open_bak" ] && sysctl -w fs.nr_open=$(cat $keentune_tmp_dir/nr_open_bak)
[ -s "${nginx_conf}_bak" ] && \cp ${nginx_conf}_bak $nginx_conf
systemctl stop nginx
