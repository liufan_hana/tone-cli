#!/bin/bash
nr_task=${nr_task:-1}
type=${type:-"rsa4096"}
duration=${duration:-30}
run()
{
    export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
    logger openssl speed -multi $nr_task -seconds $duration $type   | tee $TONE_BM_RESULT_DIR/openssl_$type.log 
}

parse()
{
    chmod -R 755 $TONE_BM_RESULT_DIR/*.log
    if [[ $type == "aes" ]];then
        grep '^aes-128' $TONE_BM_RESULT_DIR/openssl_$type.log | awk 'END {
        printf("aes-128_16bytes: %s kbyte/s\n",substr($3,0,(length($3)-1)));\
        printf("aes-128_64bytes: %s kbyte/s\n",substr($4,0,(length($4)-1)));\
        printf("aes-128_256bytes: %s kbyte/s\n",substr($5,0,(length($5)-1)));\
        printf("aes-128_1024bytes: %s kbyte/s\n",substr($6,0,(length($6)-1)));\
        printf("aes-128_8192bytes: %s kbyte/s\n",substr($7,0,(length($7)-1)));\
        printf("aes-128_16384bytes: %s kbyte/s\n",substr($8,0,(length($8)-1)))}'
        grep '^aes-192' $TONE_BM_RESULT_DIR/openssl_$type.log | awk 'END {
        printf("aes-192_16bytes: %s kbyte/s\n",substr($3,0,(length($3)-1)));\
        printf("aes-192_64bytes: %s kbyte/s\n",substr($4,0,(length($4)-1)));\
        printf("aes-192_256bytes: %s kbyte/s\n",substr($5,0,(length($5)-1)));\
        printf("aes-192_1024bytes: %s kbyte/s\n",substr($6,0,(length($6)-1)));\
        printf("aes-192_8192bytes: %s kbyte/s\n",substr($7,0,(length($7)-1)));\
        printf("aes-192_16384bytes: %s kbyte/s\n",substr($8,0,(length($8)-1)))}'
        grep '^aes-256' $TONE_BM_RESULT_DIR/openssl_$type.log | awk 'END {
        printf("aes-256_16bytes: %s kbyte/s\n",substr($3,0,(length($3)-1)));\
        printf("aes-256_64bytes: %s kbyte/s\n",substr($4,0,(length($4)-1)));\
        printf("aes-256_256bytes: %s kbyte/s\n",substr($5,0,(length($5)-1)));\
        printf("aes-256_1024bytes: %s kbyte/s\n",substr($6,0,(length($6)-1)));\
        printf("aes-256_8192bytes: %s kbyte/s\n",substr($7,0,(length($7)-1)));\
        printf("aes-256_16384bytes: %s kbyte/s\n",substr($8,0,(length($8)-1)))}'
    elif [[ $type == "des" ]]; then
        grep '^des cbc' $TONE_BM_RESULT_DIR/openssl_$type.log | awk 'END {
        printf("des-cbc_16bytes: %s kbyte/s\n",substr($3,0,(length($3)-1)));\
        printf("des-cbc_64bytes: %s kbyte/s\n",substr($4,0,(length($4)-1)));\
        printf("des-cbc_256bytes: %s kbyte/s\n",substr($5,0,(length($5)-1)));\
        printf("des-cbc_1024bytes: %s kbyte/s\n",substr($6,0,(length($6)-1)));\
        printf("des-cbc_8192bytes: %s kbyte/s\n",substr($7,0,(length($7)-1)));\
        printf("des-cbc_16384bytes: %s kbyte/s\n",substr($8,0,(length($8)-1)))}'
        grep '^des ede3' $TONE_BM_RESULT_DIR/openssl_$type.log | awk 'END {
        printf("des-ede3_16bytes: %s kbyte/s\n",substr($3,0,(length($3)-1)));\
        printf("des-ede3_64bytes: %s kbyte/s\n",substr($4,0,(length($4)-1)));\
        printf("des-ede3_256bytes: %s kbyte/s\n",substr($5,0,(length($5)-1)));\
        printf("des-ede3_1024bytes: %s kbyte/s\n",substr($6,0,(length($6)-1)));\
        printf("des-ede3_8192bytes: %s kbyte/s\n",substr($7,0,(length($7)-1)));\
        printf("des-ede3_16384bytes: %s kbyte/s\n",substr($8,0,(length($8)-1)))}'
    elif [[ $type == "dsa2048" ]]; then
        grep '^dsa '| awk 'END {printf("dsa2048_sign: %s sign/s\n",$(NF-1));printf("dsa2048_verify: %s verify/s\n",$NF)}'
    elif [[ $type == "rsa4096" ]]; then
        grep -A 1 'sign    verify    sign/s verify/s'| awk 'END {printf("rsa4096_sign: %s sign/s\n",$(NF-1));printf("rsa4096_verify: %s verify/s\n",$NF)}'
    elif [[ $type == "sha256" ]]; then
        grep 'sha256  ' | awk 'END {printf("sha256_16bytes: %s kbyte/s\n",substr($2,0,(length($2)-1)));\
        printf("sha256_64bytes: %s kbyte/s\n",substr($3,0,(length($3)-1)));\
        printf("sha256_256bytes: %s kbyte/s\n",substr($4,0,(length($4)-1)));\
        printf("sha256_1024bytes: %s kbyte/s\n",substr($5,0,(length($5)-1)));\
        printf("sha256_8192bytes: %s kbyte/s\n",substr($6,0,(length($6)-1)));\
        printf("sha256_16384bytes: %s kbyte/s\n",substr($7,0,(length($7)-1)));}'
    else
        echo "Unsupported type" && return 1
    fi
}
