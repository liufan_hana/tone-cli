# Avaliable environment:
#
# Download variable:Download variable
GITLAT_URL="gitee.com:anolis/ancert.git"
BM_NAME="ancert"
DEP_PKG_LIST="git make gcc gcc-c++ fio nvme-cli glx-utils python3 rpm-build bc lvm2 alsa-lib alsa-utils virt-what smartmontools hdparm xorg-x11-utils xorg-x11-server-utils xorg-x11-apps"

fetch()
{
    local code_repo="git@$GITLAT_URL"
    git_clone $code_repo $BM_NAME

}

extract_src()
{
    :
}

build()
{
    yum install -y python-futures
    pip3 install pyyaml
}

install()
{
    cd $TONE_BM_CACHE_DIR || return
    [ -d "$TONE_BM_RUN_DIR/$BM_NAME" ] && rm -rf $TONE_BM_RUN_DIR/$BM_NAME
    cp -af $BM_NAME/$BM_NAME $TONE_BM_RUN_DIR
}

uninstall()
{
    rm -rf $TONE_BM_RUN_DIR/$BM_NAME
    rm -rf $TONE_BM_CACHE_DIR/$BM_NAME
}
