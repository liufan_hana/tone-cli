#!/usr/bin/awk -f

/kB  reclen.*/{
getline
printf("kB: %d\n",$1)
printf("reclen: %d\n",$2)
printf("write: %d\n",$3)
printf("rewrite: %d\n",$4)
printf("read: %d\n",$5)
printf("reread: %d\n",$6)
printf("random_read: %d\n",$7)
printf("random_write: %d\n",$8)
}
