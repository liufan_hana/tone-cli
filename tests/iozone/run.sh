#!/bin/sh
[[ ! $IOZONE_FILEPATH ]] && filepath="/home" || filepath=$IOZONE_FILEPATH
testfile=$filepath/iozone.data

run()
{
    available_file_sz=$(df | grep -w $filepath | awk '{print $4}')
    test_file_sz=$(echo "$filesize" | awk -F 'G' '{print $1}')
    test_file_bits=$(expr $test_file_sz \* 1024 \* 1024)
    if [ $test_file_bits -le $available_file_sz ];then
        cd $TONE_BM_RUN_DIR/iozone3_492/src/current/
        logger ./iozone -a -s ${filesize} -r ${recordsize} -f $testfile
        echo "$(date) iozone end ${filesize}"
        logger sync
        logger "echo 3 > /proc/sys/vm/drop_caches"
        sleep 30
    else
        echo "ERROR:Insufficient system disk space"
        [ $test_file_bits -le $available_file_sz  ]
        exit 1
    fi
}

teardown()
{
    rm -f $testfile || true
}


parse()
{
    $TONE_BM_SUITE_DIR/parse.awk
}
