#!/bin/bash

ltp_blacklist=$TONE_BM_SUITE_DIR/ltp.blacklist
sctp_flag=0

prepare_for_blacklist()
{
	# tpci testcase run on ecs will let the machine hanged, skip it
	systemd-detect-virt --vm -q && grep -q "^tpci$" $ltp_blacklist || echo "tpci" >>$ltp_blacklist
}

setup_for_net()
{	
	lsmod | grep -q sctp || modprobe sctp && sctp_flag=1	
}

cleanup_for_net()
{	
	if [ $sctp_flag == 1 ]; then
		modprobe -r sctp
	fi
}

setup()
{
	echo 1    > /proc/sys/kernel/panic
	echo 1  > /proc/sys/kernel/hardlockup_panic
	echo 1    > /proc/sys/kernel/softlockup_panic
	echo 60   > /proc/sys/kernel/watchdog_thresh
	echo 0    > /proc/sys/kernel/hung_task_panic
	echo 1200 > /proc/sys/kernel/hung_task_timeout_secs
	systemctl restart kdump.service
	systemctl status kdump.service |grep 'Active: active'
	[ $? -ne 0 ] && echo "the status of kdump.service is abnormal" && exit 1
	prepare_for_blacklist
	setup_for_net
	mkdir -p /tmp/ltp_tmpdir
}

run()
{
	grep 'SCENARIO_LISTS="$LTP.*network' runltp && sed -i 's|$LTP.*network|$SCENARIO_LISTS &|g' runltp
	nr_cpu=$(nproc)
	mem_kb=$(grep ^MemTotal /proc/meminfo | awk '{print $2}')
	start_time=$(cat /proc/uptime |awk -F'.' '{print $1}')
	nr_cpu_c=$((nr_cpu / 2))
	[ $nr_cpu_c -eq 0 ] && nr_cpu_c=1
	nr_cpu_m=$((nr_cpu / 4))
	[ $nr_cpu_m -eq 0 ] && nr_cpu_m=1
	logger ./runltp \
		-c $nr_cpu_c \
		-m $nr_cpu_m,1,$(((mem_kb / 2) / nr_cpu_m * 1024)) \
		-D 1,1,0,1 \
		-B ${LTP_DEV_FS:-ext4} \
		-R -p -q \
		-N \
		-t $runtime \
		-d ${LTP_TMPDIR:-/tmp/ltp_tmpdir} \
		-S $ltp_blacklist
	check_result
}

check_result()
{
	end_time=$(cat /proc/uptime |awk -F'.' '{print $1}')
	total_runtime=$(expr $end_time - $start_time )
	# kill all test processes need some time
	sleep 5
	check_kmemleak=$(dmesg |grep -i 'new suspected memory leaks')
	check_process_z=$(ps -A -ostat,ppid,pid,cmd |grep -e '^[Zz]')
	check_process_d=$(ps -A -ostat,ppid,pid,cmd |grep '^D')
	if [ "$total_runtime" -lt "$runtime" ];then
		echo "[ltp-stress_result] time_check Fail"
		echo "end_time=$end_time start_time=$start_time"
	else
		echo "[ltp-stress_result] time_check Pass"
	fi
	if [ xx"$check_process_z" == xx ];then
		echo "[ltp-stress_result] process_z_check Pass"
	else
		echo "[ltp-stress_result] process_z_check Fail"
		ps -A -ostat,ppid,pid,cmd |grep -e '^[Zz]'|grep -v grep
	fi
	if [ xx"$check_process_d" == xx ];then
		echo "[ltp-stress_result] process_d_check Pass"
	else
		echo "[ltp-stress_result] process_d_check Fail"
		ps -A -ostat,ppid,pid,cmd |grep '^D' |grep -v grep
	fi
	if [ xx"$check_kmemleak" == xx ];then
		echo "[ltp-stress_result] kmemleak_check Pass"
	else
		echo "[ltp-stress_result] kmemleak_check Fail"
	fi
	cat /var/log/messages |grep 'ltp-stress' |grep -i 'error'
	if [ $? -ne 0 ];then
		echo "[ltp-stress_result] messages_check Pass"
	else
		echo "[ltp-stress_result] messages_check Fail"
	fi	
}

parse()
{
	awk -f $TONE_BM_SUITE_DIR/parse.awk
}

teardown()
{
	cleanup_for_net
	rm -rf /tmp/ltp_tmpdir
	exit 0
}
