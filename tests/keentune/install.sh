#!/bin/bash

TONE_CAN_SKIP_PKG="yes"
DEP_PKG_LIST="python36 python36-devel golang rust nginx openssl-devel git kernel-headers"

nginx_conf=/etc/nginx/nginx.conf

function version_ge()
{
    test "$(echo "$@" | tr " " "\n" | sort -rV | head -n 1)" == "$1"; 
}

check_kernel_headers()
{
    rpm -qa|grep kernel-headers-$(uname -r) && return 0
    local version=$(yum list --showduplicates  kernel-headers-`uname -r` |tail -1|awk '{print $2}')
    [ -n "$version" ] && yum install -y kernel-headers-$version && return 0 || return 1
}

check_go_version()
{
    local cur_version=$(go version |awk '{print $3}')
    local min_version="1.15"
    if [ -n "$cur_version" ]; then
        version_ge "$cur_version" "$min_version" && return 0
        yum remove -y golang
    fi
    rpm -q epel-release || yum install -y epel-release
    yum install -y golang
    
    local new_version=$(go version |awk '{print $3}')
    version_ge "$new_version" "$min_version" && return 0 || return 1
}

set_go_env()
{
    check_go_version
    go env -w GO111MODULE=on
    go env -w GOPROXY=https://goproxy.cn,direct
    go env
    go version
}

set_python_env()
{
    ulimit -n 655350
    yum install python3-systemd systemd-devel -y
    pip3 install --upgrade pip
    pip3 install Cython
    pip3 install gpytorch
    pip3 install setuptools-rust
    pip3 install pynginxconfig
    pip3 install kolla-ansible
    pip3 install kolla-ansible --ignore-installed PyYAML==5.4.1
    pip3 install torch --extra-index-url https://download.pytorch.org/whl/cpu


    pip3 install ultraopt==0.1.1
    if [ $? -ne 0 ];then
        pip3 install pillow==8.3.2
        pip3 install ultraopt==0.1.1
    fi
    
    pip3 install paramiko==2.7.2
    if [ $? -ne 0 ];then
        pip3 install cryptography==3.3
        pip3 install paramiko==2.7.2
    fi
    
    pip3 install tornado==6.1 numpy==1.19.5 POAP==0.1.26 \
        hyperopt==0.2.5 bokeh==2.3.2 requests==2.25.1 pySOT==0.3.3 \
        scikit_learn==0.24.2 shap xgboost --timeout 1800
}

check_rust()
{
    rpm -q rust && return 0
    rpm -q epel-release || yum install -y -b test epel-release
    yum install rust -y && return 0 || return 1
}

install_wrk()
{
    which wrk && return 0
    git clone https://gitee.com/mirrors/wrk.git wrk
    cd wrk
    make && cp wrk /usr/bin/
    cd -
}

set_nginx_env()
{
    [ -s ${nginx_conf}_bak ] || cp $nginx_conf ${nginx_conf}_bak
    # listen       [::]:80;
    sed -i "/^\s\+listen\s\+\[::\]/d" $nginx_conf
    # access_log  /var/log/nginx/access.log  main;
    sed -i "s/access_log.*/access_log off;/g" $nginx_conf
    systemctl restart nginx
    systemctl status nginx || return 1
}

set_env()
{
    check_kernel_headers
    set_go_env
    check_rust
    set_python_env
}

build()
{
    set_env
    install_wrk
    set_nginx_env 
}

install()
{
    echo "install server finshed......."
}
