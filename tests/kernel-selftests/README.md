# kernel-selftests
### Description
The kernel contains a set of “self tests” under the tools/testing/selftests/ directory.


### Homepage
[cloud-kernel](https://gitee.com/anolis/cloud-kernel)

### Version
kernel_version

### Parameters
default


### Results
```
rseq.basic_percpu_ops_test: PASS
rseq.param_test: PASS
rseq.param_test_benchmark: PASS
rseq.param_test_compare_twice: PASS
rseq.run_param_test.sh: PASS
rtc.rtctest: FAIL
seccomp.seccomp_bpf: PASS
seccomp.seccomp_benchmark: PASS
sgx.test_sgx: FAIL
sigaltstack.sas: PASS
size.get_size: PASS
splice.default_file_splice_read.sh: PASS
static_keys.test_static_keys.sh: SKIP
```


### Manual Run
```
cd tools/tesing/selftests
make -C ./
./kselftest_install.sh
cd kselftest_install
./run_kselftest.sh
```

