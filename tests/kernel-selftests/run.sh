#!/bin/bash

get_kernel_info()
{
    kernel_ver=$(uname -r | awk -F '.' '{print$1"."$2}')
    os_ver=$(uname -r | awk -F '.' '{print$(NF-1)}')
    arch=$(uname -m)
}

skip_case()
{
    [ ! -f $TONE_BM_SUITE_DIR/${os_ver}_${kernel_ver}_blacklist ] && echo "not exist ${os_ver}_${kernel_ver}_blacklist file" && return  
    while read line; do
	    if echo $line |grep -q ^'#';then
	        continue
	    fi
	    if [ x"$kernel_ver" == x"4.19" ]; then
            local show_name=$(echo $line | sed 's|: |.|g')
	        cat $TONE_BM_RUN_DIR/kselftest/run_kselftest.sh | grep "$line " && echo "ignored_by_tone $show_name: Skip"
	        sed -i "/${line}$/d" $TONE_BM_RUN_DIR/kselftest/run_kselftest.sh
	        line=$(echo $line |awk -F": " '{print $NF}')
	        sed -i "/${line}\ /d" $TONE_BM_RUN_DIR/kselftest/run_kselftest.sh
	    else
            local show_name=$(echo $line | sed 's|:|.|g')
	        cat $TONE_BM_RUN_DIR/kselftest-list.txt | grep "^$line$" && echo "ignored_by_tone $show_name: Skip"
            sed -i "\|^${line}$|d" $TONE_BM_RUN_DIR/kselftest-list.txt
	    fi 
    done < $TONE_BM_SUITE_DIR/${os_ver}_${kernel_ver}_blacklist
}

setup()
{
    get_kernel_info
    skip_case

    # prepare for bpf
    [ -d $TONE_BM_RUN_DIR/bpf ] && echo "timeout=100" > $TONE_BM_RUN_DIR/bpf/settings
}

run ()
{
    # run kernel_selftests
    cd $TONE_BM_RUN_DIR/kselftest/
    if [ `uname -r | grep '4\.19'| grep -E "an8|al8"` ];then
       # https://bugzilla.openanolis.cn/show_bug.cgi?id=3088
       cat run_kselftest.sh | grep 'cpufreq: main' && echo "cpufreq: main.sh: Skip"
       sed -i "/main.sh/d" ./run_kselftest.sh
       ./run_kselftest.sh
    else
       [ -f ./run_kselftest.sh ] && ./run_kselftest.sh || ../run_kselftest.sh
    fi
}

parse ()
{
    $TONE_BM_SUITE_DIR/parse.py
}
    
