GIT_URL="https://gitee.com/anolis/cloud-kernel.git"
DEP_PKG_LIST="gcc make bc patch rsync iproute-tc llvm-toolset glibc-static popt-devel libcap-ng-devel libcap-devel fuse-devel fuse numactl-devel elfutils-libelf-devel libmount-devel libmnl-devel openssl-devel libhugetlbfs-devel gcc-c++ userspace-rcu-devel clang llvm alsa-lib alsa-lib-devel"
TONE_CAN_SKIP_PKG=yes

get_kernel_version() {
    version=$(uname -r | awk -F "-" '{print $1}')
    echo ${version%.*}
}

kernel_source_dir="kernel"
kernel_version=$(get_kernel_version)
kernel_version_int=$(echo ${kernel_version%%.*})

if [ -n "$KERNEL_BRANCH" ]; then
    BRANCH="$KERNEL_BRANCH"
else
    if [ ${kernel_version_int} -le 4 ]; then
        kernel_source_dir="kernel-4"
        BRANCH="devel-4.19"
    elif [ ${kernel_version_int} -eq 5 ];then
        kernel_source_dir="kernel-5"
        BRANCH="devel-5.10"
    else
        BRANCH="master"
    fi
fi

fetch() {
    [ -d $TONE_BM_CACHE_DIR/$kernel_source_dir ] && rm -rf $TONE_BM_CACHE_DIR/$kernel_source_dir/* || mkdir -p $TONE_BM_CACHE_DIR/$kernel_source_dir
    if [ -n "$KERNEL_SRC" ]; then
	    if echo "$KERNEL_SRC" |grep -E "src.rpm|\.tar"; then
            wget -T 30 -q $KERNEL_SRC -P $TONE_BM_CACHE_DIR/$kernel_source_dir || echo "pls check the format of $KERNEL_SRC"
        elif echo "$KERNEL_SRC" |grep ".git"; then
	        GIT_URL="$KERNEL_SRC"
	        unset KERNEL_SRC 
        fi
    fi
    if [ -z "$KERNEL_SRC" ] && [ -n "$GIT_URL" ]; then
        timeout 60m git clone -q --depth 1 -b $BRANCH $GIT_URL $TONE_BM_CACHE_DIR/$kernel_source_dir
    fi
}

extract_src()
{
    [ -d $TONE_BM_BUILD_DIR/$kernel_source_dir ] && rm -rf $TONE_BM_BUILD_DIR/$kernel_source_dir/* || mkdir -p $TONE_BM_BUILD_DIR/$kernel_source_dir
    if [ -n "$KERNEL_SRC" ]; then
	    [ -d ~/rpmbuild ] && rm -rf ~/rpmbuild
        [ -d /rpmbuild ] && rm -rf /rpmbuild
        kernel_src_rpm=$(echo $KERNEL_SRC |awk -F '/' '{print $NF}')
	    if echo "$KERNEL_SRC" |grep "src.rpm"; then
	        rpm -ivh $TONE_BM_CACHE_DIR/$kernel_source_dir/$kernel_src_rpm
	        cd $TONE_BM_CACHE_DIR/$kernel_source_dir/
	        rpm2cpio $kernel_src_rpm |cpio -di 
	        tar xvf linux-*.tar.xz --strip-component 1 &>/dev/null 
	        cd -
        elif echo "$KERNEL_SRC" |grep ".tar"; then
	        tar xvf $TONE_BM_CACHE_DIR/$kernel_source_dir/$kernel_src_rpm --strip-component 1 -C $TONE_BM_CACHE_DIR/$kernel_source_dir &> /dev/null
        fi
        [ -d ~/rpmbuild ] && rpmbuild_dir=~/rpmbuild || rpmbuild_dir=/rpmbuild
        which yum-builddep || yum install -y yum-utils
        which rpmbuild || yum install -y rpm-build
        logger yum-builddep -y $rpmbuild_dir/SPECS/kernel.spec
        logger rpmbuild -bp $rpmbuild_dir/SPECS/kernel.spec
        cp -af $rpmbuild_dir/BUILD/kernel-*/linux*/* $TONE_BM_BUILD_DIR/$kernel_source_dir
    fi
    if [ -z "$KERNEL_SRC" ] && [ -n "$GIT_URL" ]; then
	    git_clone_exec --branch "$BRANCH" "$TONE_BM_CACHE_DIR/$kernel_source_dir" "$TONE_BM_BUILD_DIR/$kernel_source_dir"
    fi
}

build() {

    cd $TONE_BM_CACHE_DIR/$kernel_source_dir
    patchpath="$TONE_BM_SUITE_DIR/patch/$kernel_version"

    # subcase return code  not 0,2 --> exitcode=1
    [ -f $patchpath/pmtu.sh.patch ] && patch -p1 < $patchpath/pmtu.sh.patch
    # fix kernel change
    [ -f $patchpath/mincore.patch ] && patch -p1 < $patchpath/mincore.patch

    make -C tools/testing/selftests
}

prepare_skip_list() {

    # skip breakpoints case if arch is x86_64
    [ "$(uname -m)" == "x86_64" ] && skip_test_list="breakpoints"
    # skip ftrace case if arch is 5.10 x86_64
    # https://bugzilla.openanolis.cn/show_bug.cgi?id=1253
    [ "$kernel_version" == "5.10" -a "$(uname -m)" == "x86_64" ] && skip_test_list="$skip_test_list ftrace"
    # skip gpio test if CONFIG_GPIO_MOCKUP is not set
    grep CONFIG_GPIO_MOCKUP /boot/config-$(uname -r) | grep -q "is not set" && skip_test_list="$skip_test_list gpio"
    # skip dma_heap test if CONFIG_DMABUF_HEAPS is not set
    grep CONFIG_DMABUF_HEAPS /boot/config-$(uname -r) | grep -q "is not set" && skip_test_list="$skip_test_list dmabuf-heaps"
    echo "the final skip_test_list is : $skip_test_list"

    # skip install test
    for test in $skip_test_list; do
        sed -i "/$test/ s/^/#/" Makefile
    done
}

install() {
    cd $TONE_BM_CACHE_DIR/$kernel_source_dir/tools/testing/selftests/
    prepare_skip_list
    ./kselftest_install.sh $TONE_BM_RUN_DIR
}
