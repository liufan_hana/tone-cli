#!/usr/bin/awk -f

BEGIN {
	nr_pass = 0
	nr_fail = 0
	nr_skip = 0
}

/^tag=/ {
	tc_name = substr($1,5)
}

/termination_type=exited termination_id=0/ {
	tc_ret="pass"
	nr_pass++
}

/termination_type=exited termination_id=[1-9]+/ {
	tc_ret="fail"
	nr_fail++
}

/termination_type=exited termination_id=32/ {
	tc_ret="skip"
	nr_fail--
	nr_skip++
}

/<<<test_end>>>/ {
	if ( tc_ret == "pass" ) {
		printf("%s: pass\n", tc_name)
	} else if ( tc_ret == "fail" ) {
		printf("%s: fail\n", tc_name)
	} else if ( tc_ret == "skip" ) {
		printf("%s: skip\n", tc_name)
	} else {
		printf("%s: unknown\n", tc_name)
	}
}
