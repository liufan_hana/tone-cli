. $TONE_BM_SUITE_DIR/pts-disk.sh
PTS_URL="https://ostester.oss-cn-hangzhou.aliyuncs.com/benchmarks/phoronix-test-suite-10.8.4.tar"
PTS_CACHE="https://ostester.oss-cn-hangzhou.aliyuncs.com/benchmarks/pts-download-cached.tar"

DEP_PKG_LIST="php php-xml php-cli zlib-devel nmap perl-IPC-Cmd python3 expat-devel"

grep -q Anolis /etc/os-release && DEP_PKG_LIST+=" php-json nmap-ncat"
free_disk=$(df -BG $TONE_BM_RUN_DIR | tail -1 | awk '{print $4}'  | sed 's/G//')


fetch()
{
        echo "Download phoronix-test-suite-10.8.4.tar"
        wget -t 3 -T 300 --no-clobber $PTS_URL
        echo "Download pts-download-cached.tar"
        wget -t 3 -T 600 --no-clobber $PTS_CACHE
}


build()
{
        echo "Skip build phoronix-test-suite"
        if [ "$free_disk" -lt 20 ]; then
                #Check available data disks
                [[ -n $PTS_TESTS_INSTALL_DIR ]] || mkdir -p /home/pts-test && PTS_TESTS_INSTALL_DIR=/home/pts-test
                if [[ ! -d $PTS_TESTS_INSTALL_DIR  ]]; then
                        echo " ERROR : $PTS_TESTS_INSTALL_DIR Folder does not exist "
                        exit 1
                fi
		init_disk
        else
                [[ -n $PTS_TESTS_INSTALL_DIR ]] || PTS_TESTS_INSTALL_DIR=$TONE_BM_RUN_DIR
        fi
}

init_disk()
{
        get_test_disk
        logger wipefs -a -f ${test_disk}
        logger partprobe ${test_disk}
        logger parted -s ${test_disk} mklabel gpt mkpart primary 0% 100%
        sleep 3
        test_disk="/dev/$(lsblk -b -P -o 'NAME','TYPE' ${test_disk} | grep 'part' | awk -F"[= ]" '{print $2}' |sed 's/"//g')"
        logger mkfs -t ext4 -E lazy_itable_init=0,lazy_journal_init=0 -L tone -q -F ${test_disk}
        logger mount -t ext4  ${test_disk} $PTS_TESTS_INSTALL_DIR

}

install()
{
        logger "tar xvf $TONE_BM_CACHE_DIR/phoronix-test-suite-10.8.4.tar -C $PTS_TESTS_INSTALL_DIR"
        logger "tar xvf $TONE_BM_CACHE_DIR/pts-download-cached.tar -C $PTS_TESTS_INSTALL_DIR"
        mkdir -p $PTS_TESTS_INSTALL_DIR/{installed-tests,test-results}
        cd $PTS_TESTS_INSTALL_DIR/phoronix-test-suite
        logger ./phoronix-test-suite user-config-set \
                EnvironmentDirectory=$PTS_TESTS_INSTALL_DIR/installed-tests \
                CacheDirectory=$PTS_TESTS_INSTALL_DIR/download-cache \
                ResultsDirectory=$PTS_TESTS_INSTALL_DIR/test-results \
                SaveResults=TRUE \
                OpenBrowser=FALSE \
                UploadResults=FALSE \
                PromptForTestIdentifier=FALSE \
                PromptForTestDescription=FALSE \
                PromptSaveName=FALSE \
                RunAllTestCombinations=FALSE \
                Configured=TRUE
}
