#!/bin/bash
. $TONE_ROOT/lib/disk.sh
check_test_size()
{
        local disk_name=$1
        local test_size=$2
        # limit test_size under available disk size
        [[ $(size_cn_bytes $test_size) -gt $(lsblk -b -P -o 'SIZE' ${disk_name} | head -1 | egrep -o '[0-9]+') ]] && return 1 || return 0
}

get_test_disk()
{
        local type_disk=
        local type_part=
        local test_size=40G

        for disk_name in $(lsblk -b -P -o 'NAME' | awk -F"=" '{print $2}' | sed 's/\"//g' | sort) ;do
        if $(lsblk -b -P -o 'TYPE' /dev/${disk_name} | grep -q 'disk');then
            type_disk="$type_disk /dev/${disk_name}"
        elif $(lsblk -b -P -o 'TYPE' /dev/${disk_name} | grep -q 'part');then
            type_part="$type_part /dev/${disk_name}"
        fi
        done

        local usable_disks=${type_disk}
        for disk in $(echo ${type_disk} | xargs -n 1);do
                if ! $(check_test_size ${disk} ${test_size}); then
                usable_disks=$(echo ${usable_disks} | sed "s|$disk||" )
                echo "${disk} less than testsize"
                fi
        done

        local unusable_parts=
        for part in $(echo ${type_part} | xargs -n 1);do
                if ! $(lsblk -b -P -o 'MOUNTPOINT' ${part} | grep -q '\"\"')  ;then
                unusable_parts="${unusable_parts} ${part}"
                echo "${part} already mounted"
                fi
        done

        for usable_disk in $(echo ${usable_disks} | xargs -n 1); do
                if ! $(echo ${unusable_parts} | grep -q ${usable_disk}); then
                        echo "${usable_disk} will be used for pts test"
                        test_disk=${usable_disk}
                        return 0
                fi
        done
        message="Please check that there are free disks in the test environment"
        [[ -z $test_disk ]] && echo "ERROR:$message" && exit 1

}

check_testdisk()
{
    local test_disk=$1
    local test_size=$2
    check_test_size ${test_disk} ${test_size}
    if $(lsblk -b -P -o 'MOUNTPOINT' ${test_disk} | grep -q '"\/"');then
        echo "${test_disk} is Linux’s root(/), test abort"
        exit 1
    fi
    mount | grep -q ${test_disk} && umount ${test_disk}
}

