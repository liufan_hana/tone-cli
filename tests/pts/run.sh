#!/bin/bash
# - bench
. $TONE_BM_SUITE_DIR/pts-disk.sh
free_disk=$(df -BG $TONE_BM_RUN_DIR | tail -1 | awk '{print $4}'  | sed 's/G//')

init_disk()
{
        get_test_disk
        logger wipefs -a -f ${test_disk}
        logger partprobe ${test_disk}
        logger parted -s ${test_disk} mklabel gpt mkpart primary 0% 100%
        sleep 3
        test_disk="/dev/$(lsblk -b -P -o 'NAME','TYPE' ${test_disk} | grep 'part' | awk -F"[= ]" '{print $2}' |sed 's/"//g')"
        logger mount -t ext4  ${test_disk} $PTS_TESTS_INSTALL_DIR

}

configure_benchmark()
{
        nr_cpus=$(nproc)
        bench_opts="$TONE_BM_SUITE_DIR/options-$bench"

        case $bench in
                john-the-ripper) version=1.7.2;;
                openssl) version=1.11.0;;
                compress-gzip) version=1.2.0;;
                compress-7zip) version=1.9.0;;
                compilebench) version=1.0.3;;
                phpbench) version=1.1.6;;
                pgbench) version=1.11.1;;
                sqlite) version=2.1.0;;
                redis) version=1.3.1;;
                nginx) version=3.0.0;;
                apache) version=2.0.1;;
                sysbench) version=1.1.0;;
                network-loopback) version=1.0.3;;
                postmark) version=1.1.2;;
                iozone) version=1.9.6;;
                dbench) version=1.0.2;;
                tiobench) version=1.3.1;;
                blogbench) version=1.1.0;;
                fs-mark) version=1.0.3;;
                fio) version=1.15.0;;
                ramspeed) version=1.4.3;;
                stream) version=1.3.3;;
                cachebench) version=1.1.2;;
                c-ray) version=1.2.0;;
                mafft) version=1.6.2;;
        esac

        if [ "x$bench" = "xsqlite" ]; then
                if [ "$nr_cpus" -lt 2 ]; then
                        echo "1" > $bench_opts
                elif [ "$nr_cpus" -le 8 ]; then
                        echo "1,2" > $bench_opts
                elif [ "$nr_cpus" -le 32 ]; then
                        echo "1,2,3" > $bench_opts
                elif [ "$nr_cpus" -le 64 ]; then
                        echo "1,3,4" > $bench_opts
                else
                        echo "1,4,5" > $bench_opts
                fi
        fi

        if [ "x$bench" = "xfs-mark" ]; then
                if [ "$free_disk" -lt 20 ]; then
                        echo "Insufficient free disk space to run fs-mark"
                        return 1
                elif [ "$free_disk" -gt 100 ]; then
                        echo "5" > $bench_opts
                elif [ "$free_disk" -gt 40 ]; then
                        echo "1,4" > $bench_opts
                else
                        echo "1" > $bench_opts
                fi
        fi

        if [ "x$bench" = "xnginx" -o "x$bench" = "xapache" ]; then
                if [ "$nr_cpus" -le 16 ]; then
                        echo "2" > $bench_opts
                elif [ "$nr_cpus" -le 64 ]; then
                        echo "3" > $bench_opts
                elif [ "$nr_cpus" -le 128 ]; then
                        echo "4" > $bench_opts
                else
                        echo "5" > $bench_opts
                fi
        fi
}

setup()
{
	if [ "$free_disk" -lt 20 ]; then
        	echo "Check available data disks"
      		[[ -n $PTS_TESTS_INSTALL_DIR ]] ||  PTS_TESTS_INSTALL_DIR=/home/pts-test
      		if [[ ! -d $PTS_TESTS_INSTALL_DIR  ]]; then
          		echo " ERROR : $PTS_TESTS_INSTALL_DIR Folder does not exist "
          		exit 1
      		fi
		mnt_mount=$(df -BG | grep $PTS_TESTS_INSTALL_DIR | awk '{print $1}')
        	[[ -z $mnt_mount ]] && init_disk
	else
      		[[ -n $PTS_TESTS_INSTALL_DIR ]] || PTS_TESTS_INSTALL_DIR=$TONE_BM_RUN_DIR
	fi
	echo "$test_disk will be used for pts test"
        sleep 3
        configure_benchmark || return 1
        # make sure benchmark test options exist
        ls $bench_opts || return 1

        if [ "x$bench" = "xblogbench" ]; then
                if [ "$free_disk" -lt 15 ]; then
                        echo "Insufficient free disk space to run blogbench"
                        return 1
                fi
        fi

        if [ "x$bench" = "xiozone" ]; then
                if [ "$free_disk" -lt 10 ]; then
                        echo "Insufficient free disk space to run iozone"
                        return 1
                fi
        fi


        # install benchmark in phoronix-test-suite
        logger cd $PTS_TESTS_INSTALL_DIR/phoronix-test-suite
        logger ./phoronix-test-suite batch-install $bench-$version

        if [ "x$bench" = "xnetwork-loopback" ]; then
                nc -h | grep -q '\-delay <time>' && \
                sed -i 's/nc -d -l/nc -l/' $PTS_TESTS_INSTALL_DIR/installed-tests/pts/network-loopback-*/network-loopback
        fi
        if [ "x$bench" = "xapache" ]; then
                apache_install_dir=$(find $PTS_TESTS_INSTALL_DIR/installed-tests -name apache-*)
                if [ ! -f "$apache_install_dir/go/bin/bombardier" ]; then
                        [ $(arch) = "x86_64" ] && bombardier_name=bombardier-linux-amd64
                        [ $(arch) = "aarch64" ] && bombardier_name=bombardier-linux-arm64
                        bombardier_path=$PTS_TESTS_INSTALL_DIR/download-cache/$bombardier_name
                        [ -f "$bombardier_path" ] && {
                                mkdir -p $apache_install_dir/go/bin
                                cp $bombardier_path $apache_install_dir/go/bin/bombardier
                                chmod +x $apache_install_dir/go/bin/bombardier
                        }
                fi
        fi
}

run()
{

        logger ./phoronix-test-suite batch-run $bench-$version < $bench_opts
        upload_testlogs $(ls -d $PTS_TESTS_INSTALL_DIR/test-results/* | tail -1)

        if [ "x$bench" = "xpgbench" ]; then
                logger ./phoronix-test-suite batch-run $bench-$version < ${bench_opts}-2
                upload_testlogs $(ls -d $PTS_TESTS_INSTALL_DIR/test-results/* | tail -1)
        fi
}


parse()
{
    $TONE_BM_SUITE_DIR/parse.sh $TONE_CURRENT_RESULT_DIR/stdout.log
}

teardown()
{
    # nothing to clean up
    mnt_mount=$(echo $(df -BG | grep $PTS_TESTS_INSTALL_DIR | awk '{print $1}'))
    [[ -z $mnt_mount ]] || logger  umount -l -f $mnt_mount
    if [ "x$bench" = "xcompilebench" ]; then
        if [ ! -f /usr/bin/python ]; then 
	        if [ -f /usr/bin/python3 ]; then
	            ln -s /usr/bin/python3 /usr/bin/python
	        elif [ -f /usr/bin/python2 ]; then
		        ln -s /usr/bin/python2 /usr/bin/python
	        else
		       echo "Can Not find python in /usr/bin"
		       exit 1
	        fi
        fi
    fi
}
