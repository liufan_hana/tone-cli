WEB_URL="https://ostester.oss-cn-hangzhou.aliyuncs.com/benchmarks/rt-tests-2.4.tar.gz"
STRIP_LEVEL=1

if [[ "ubuntu debian uos kylin" =~  $TONE_OS_DISTRO ]];then
    DEP_PKG_LIST="libnuma-dev"
else
    DEP_PKG_LIST="numactl-devel"
fi

build()
{
    make
}

install()
{
    make install prefix="$TONE_BM_RUN_DIR"
}
