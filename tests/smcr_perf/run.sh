#!/bin/bash
. $TONE_ROOT/lib/common.sh
. $TONE_ROOT/lib/testinfo.sh

run() {
	echo "Run smcr test"

	[ -n "$SERVER" ] && server=${SERVER%% *} || server=127.0.0.1
	[ -n "$CLIENT" ] && client=${CLIENT%% *} || client=127.0.0.1

	if [ "x$rdma_type" == "x" ]; then
		rdma_type="erdma"
		export SMCTEST_DISABLE_ERDMA_DRIVER_INSTALL=1
	fi

	smc_args=$(echo $args | sed "s@+@ @g;s@%@\\\\x@g" | xargs -0 printf "%b")
	client_cmd="smc_test/run.sh -a $server -i $client -r client -e $rdma_type -c performance -u '$test' -p run -s '$smc_args' "
	logger $client_cmd
	cp -r smc_test/logs $TONE_CURRENT_RESULT_DIR
}

parse()
{
    awk -f $TONE_BM_SUITE_DIR/parse.awk
}
