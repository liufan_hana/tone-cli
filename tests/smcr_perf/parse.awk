

/^client.*redis.*[S|G]ET/ {
	split($0,results,"[ _ - : ]*")
	split(results[6],args,"-")
	printf("%s(%s): %s\n",args[10],results[5],results[7]);
	next
}

/^client.*connection.*:.*/ {
	split($0,results,"-")
	split(results[7],stat,"[: ]*")
	if (patsplit(results[1],proto,/smc.*$/) == 0)
		patsplit(results[1],proto,/tcp.*$/)
	patsplit(results[1],conn_type,/[^_]*_connection/)
	printf("(%s)-%s(%s): %s\n",conn_type[1],stat[1],substr(proto[1], 1, length(proto[1])-1),stat[2])
	next
}
