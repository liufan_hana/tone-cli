#!/usr/bin/awk -f

/running [0-9]+ parallel cop(y|ies) of tests/ { copies = $6 }

/^Dhrystone 2 using register variables/ && $NF ~ /[0-9.]+/  {
    printf"dhry2reg_%s: %.1f \n", copies, $NF
}
/^Double-Precision Whetstone/ && $NF ~ /[0-9.]+/ {
    printf"whetstone-double_%s: %.1f \n", copies, $NF
}
/^Execl Throughput/ && $NF ~ /[0-9.]+/ {
    printf"execl_%s: %.1f \n", copies, $NF
}
/^File Copy 1024 bufsize 2000 maxblocks/ && $NF ~ /[0-9.]+/ {
    printf"fstime_%s: %.1f \n", copies, $NF
}
/^File Copy 256 bufsize 500 maxblocks/ && $NF ~ /[0-9.]+/ {
    printf"fsbuffer_%s: %.1f \n", copies, $NF
}
/^File Copy 4096 bufsize 8000 maxblocks/ && $NF ~ /[0-9.]+/ {
    printf"fsdisk_%s: %.1f \n", copies, $NF
}
/^Pipe Throughput/ && $NF ~ /[0-9.]+/ {
    printf"pipe_%s: %.1f \n", copies, $NF
}
/^Pipe-based Context Switching/ && $NF ~ /[0-9.]+/ {
    printf"context1_%s: %.1f \n", copies, $NF
}
/^Process Creation/ && $NF ~ /[0-9.]+/ {
    printf"spawn_%s: %.1f \n", copies, $NF
}
/^Shell Scripts \(1 concurrent\)/ && $NF ~ /[0-9.]+/ {
    printf"shell1_%s: %.1f \n", copies, $NF
}
/^Shell Scripts \(8 concurrent\)/ && $NF ~ /[0-9.]+/ {
    printf"shell8_%s: %.1f \n", copies, $NF
}
/^System Call Overhead/ && $NF ~ /[0-9.]+/ {
    printf"syscall_%s: %.1f \n", copies, $NF
}
/^System Benchmarks Index Score[[:space:]]+[0-9.]+$/ {
    printf"total_score_%s: %.1f \n", copies, $NF
}

