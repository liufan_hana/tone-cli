#!/bin/bash
# - bench

run()
{
    # For fstime, fsbuffer, fsdisk tests,
    # don't create temp files on tmpfs
    df . | grep -q ^tmpfs && {
        mkdir -p /unixbench_tmp
        export UB_TMPDIR=/unixbench_tmp
    }

    export LANG=C
    #[ "$bench" = 'all' ] && bench=""

    logger ./Run
}

teardown()
{
    [ -d /unixbench_tmp ] && rm -rf /unixbench_tmp
    return 0
}

parse()
{
    $TONE_BM_SUITE_DIR/parse.awk
}
