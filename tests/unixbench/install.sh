#GIT_URL="https://github.com/kdlucas/byte-unixbench.git"
UNIXBENCH_DIR="byte-unixbench-5.1.3"
WEB_URL="https://ostester.oss-cn-hangzhou.aliyuncs.com/benchmarks/${UNIXBENCH_DIR}.tar"

if [[ "ubuntu debian uos kylin" =~ $TONE_OS_DISTRO ]]; then
    DEP_PKG_LIST="patch"
else
    DEP_PKG_LIST="patch perl-Time-HiRes"
fi

build()
{
    cd ${UNIXBENCH_DIR}
    patch_src unixbench.patch
    cd UnixBench
    if [ $(uname -m) = aarch64 ]; then
        sed -i 's/-march=native -mtune=native//g' Makefile
    fi
    make
}

install()
{
    cp -rf * $TONE_BM_RUN_DIR/
}
