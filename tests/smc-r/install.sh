#GIT_URL="https://gitee.com/openanolis/smcr_test.git"
GIT_URL="git@gitee.com:anolis/smc-test.git"

build()
{
        git submodule update --init
}

install()
{
        echo "Just copy file to run directory"
        rm -rf $TONE_BM_RUN_DIR/smcr_test
        mkdir $TONE_BM_RUN_DIR/smcr_test
        cp -r * $TONE_BM_RUN_DIR/smcr_test/
}