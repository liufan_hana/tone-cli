#!/bin/bash
. "$TONE_ROOT/lib/cpu_affinity.sh"
server_cmd="iperf3 --server --daemon"
readonly essh="ssh -o StrictHostKeyChecking=no"

[[ ! ${NUMACTL} ]] && glb_numa='numactl -C 0 -m 0' || glb_numa=${NUMACTL}

setup()
{

    export PATH="$TONE_BM_RUN_DIR"/bin:$PATH
    teardown

    firewalld_status_flag=0
    firewalld_status=`systemctl status firewalld`
    if [[ $firewalld_status =~ "active" ]];then
        systemctl stop firewalld
        firewalld_status_flag=1
    fi

    logfile=$(mktemp /tmp/iperf-server.log.XXXXXX)
    [ "$protocol" = "udp" ] && opt_udp=-u
    [ -n "$SERVER" ] && server=${SERVER%% *} || server="127.0.0.1"
    if [ "$server" = "127.0.0.1" ];then
        echo "$server ${glb_numa} $server_cmd"
        ${glb_numa} $server_cmd &> "$logfile"
        [ -n "$cpu_affinity" ] && set_cpu_affinity_server &>/dev/null
    else
        echo "$essh $server ${glb_numa} $TONE_BM_RUN_DIR/bin/$server_cmd"
        $essh "$server" ${glb_numa} "$TONE_BM_RUN_DIR/bin/$server_cmd"
    fi
    
}

run()
{
    client_cmd="${glb_numa} iperf3 -t $runtime -f M -J -c $server $opt_udp"
    # set cpu affinity if var cpu_affinity is set
    [ -n "$cpu_affinity" ] && set_cpu_affinity_client &>/dev/null
    echo "${client_cmd}"
    ${client_cmd} | tee 2>&1 "$TONE_CURRENT_RESULT_DIR"/${testsuite}.json
}

parse()
{
    "$TONE_BM_SUITE_DIR"/parse.py
}

teardown()
{
    pkill iperf3 &>/dev/null
    rm -rf /tmp/iperf-server.log.*
    sleep 2
    if [ $firewalld_status_flag -eq 1 ];then
        systemctl start firewalld
    fi

}
