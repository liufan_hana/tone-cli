#!/bin/bash -x

. $TONE_ROOT/lib/disk.sh
use_loop_dev_flag=0
kernel_ver=$(uname -r | awk -F '.' '{print$1"."$2}')
os_ver=$(uname -r | awk -F '.' '{print$(NF-1)}')

add_user()
{
    [ -n "$1" ] || return
    grep -q -w "$1" /etc/passwd && return
    useradd $1
}

prepare_disk_for_test()
{
    nr_disk=4
    if [[ "$fs" == ext4 ]]; then
        export mkfsopt=" -q -F"
    elif [ $fs  == xfs ]; then
        export mkfsopt=" -f "
    else
        export mkfsopt=""
    fi

    if [ -n "$TEST_DISK" ]; then
        test_disk_list=($(echo $TEST_DISK|tr ',' ' '))
    else
        test_disk_list=($(eval get_test_disks))
    fi

    disk_num=${#test_disk_list[*]}
    if [ $disk_num -gt 0 ] && [ $disk_num -lt $nr_disk ]; then
        setup_disk_fs ${test_disk_list[0]} "$nr_disk" "mounted"
        disk_list=($(eval get_dev_partition))
        mounted_list=($(eval get_mount_points))
        umount_fs
        export FSTYP=$fs
        export TEST_DEV="${disk_list[0]}"
        export SCRATCH_DEV="${disk_list[1]}"
        export TEST_LOGDEV="${disk_list[2]}"
        export SCRATCH_LOGDEV="${disk_list[3]}"
        export TEST_DIR=${mounted_list[0]}
        export SCRATCH_MNT=${mounted_list[1]}
    elif [ $disk_num -ge $nr_disk ]; then
        mkfs.$fs $mkfsopt ${test_disk_list[0]}
        mkfs.$fs $mkfsopt ${test_disk_list[1]}
        mkfs.$fs $mkfsopt ${test_disk_list[2]}
        mkfs.$fs $mkfsopt ${test_disk_list[3]}
        mkdir -p /mnt/test /mnt/scratch
        export FSTYP=$fs
        export TEST_DEV="${test_disk_list[0]}"
        export SCRATCH_DEV="${test_disk_list[1]}"
        export TEST_LOGDEV="${test_disk_list[2]}"
        export SCRATCH_LOGDEV="${test_disk_list[3]}"
        export TEST_DIR=/mnt/test
        export SCRATCH_MNT=/mnt/scratch
    else
        # 无空闲磁盘时判断根分区剩余空间，小于30G报错，退出
        need_disk_size=30720 #单位M，30G
        disk_free_size=$(df -m|grep -w "/"|awk '{print $4}')
        if [ $disk_free_size -lt $need_disk_size ]; then
            echo "The system has no free disk, so using the loop device to test requires at least 30720M(30G) of space. The current system disk has only ${disk_free_size}M of space, so the test cannot be performed."
            exit 1
        fi
        use_loop_dev_flag=1
        for index in $(seq 90 93); do
            [ -e /dev/loop$index ] && continue
            mknod -m 0660 /dev/loop$index b 7 $index
            chown root:disk /dev/loop$index
        done
        dd if=/dev/zero of=/tmp/loop90.img bs=1G count=10
        dd if=/dev/zero of=/tmp/loop91.img bs=1G count=10
        dd if=/dev/zero of=/tmp/loop92.img bs=1G count=5
        dd if=/dev/zero of=/tmp/loop93.img bs=1G count=5

        for index in $(seq 90 93); do
            mkfs.$fs $mkfsopt /tmp/loop${index}.img
            losetup /dev/loop${index} /tmp/loop${index}.img
        done

        mkdir -p /mnt/test /mnt/scratch
        export FSTYP=$fs
        export TEST_DEV=/dev/loop90
        export SCRATCH_DEV=/dev/loop91
        export TEST_LOGDEV=/dev/loop92
        export SCRATCH_LOGDEV=/dev/loop93
        export TEST_DIR=/mnt/test
        export SCRATCH_MNT=/mnt/scratch
    fi
    echo "test conf:"
    echo "FSTYP is: " $FSTYP
    echo "TEST_DEV is: " $TEST_DEV
    echo "SCRATCH_DEV is: " $SCRATCH_DEV
    echo "TEST_LOGDEV is: " $TEST_LOGDEV
    echo "SCRATCH_LOGDEV is: " $SCRATCH_LOGDEV
    echo "TEST_DIR is: " $TEST_DIR
    echo "SCRATCH_MNT is: " $SCRATCH_MNT
}

setup()
{
    add_user "fsgqa"
    add_user "123456-fsgqa"
    add_user "fsgqa2"
    prepare_disk_for_test
}

run()
{
    local opts=""
    local kver=$(uname -r | cut -d. -f1-2)
    [ -d "blacklists" ] || mkdir blacklists
    [ -f "blacklists/exclude" ] && rm -f blacklists/exclude
    [ -f $TONE_BM_SUITE_DIR/blacklists/${fs}-exclude ] && \
        cat $TONE_BM_SUITE_DIR/blacklists/${fs}-exclude > blacklists/exclude
    [ -f $TONE_BM_SUITE_DIR/blacklists/${fs}-exclude-${kver} ] && \
        cat $TONE_BM_SUITE_DIR/blacklists/${fs}-exclude-${kver} >> blacklists/exclude
    [ -f "blacklists/exclude" ] && opts="$opts -E blacklists/exclude"
    ./check $opts -g "auto"
}

parse()
{
    $TONE_BM_SUITE_DIR/parse.awk
}

teardown()
{
    if [ $use_loop_dev_flag -eq 1 ]; then
        losetup -d /dev/loop90
        losetup -d /dev/loop91
        losetup -d /dev/loop92
        losetup -d /dev/loop93
        rm -rf /tmp/loop90.img /tmp/loop91.img /tmp/loop92.img /tmp/loop93.img
        rm -rf /dev/loop90 /dev/loop91 /dev/loop92 /dev/loop93
    fi
}
