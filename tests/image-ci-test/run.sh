#!/bin/bash
# Avaliable environment variables:
# $C_VERSION: image version seperated by '#'
# $CONTAINER_ENGINE: container engine for func test seperated by '/'


run()
{
    [ -n "$group" ] || {
        echo "No group in specfile"
        return 1
    }
    export PYTHONPATH=$TONE_BM_RUN_DIR/image_ci/tests
    registry_type=(`echo $REGISTRY_TYPE | tr '/' ' '`)
    if [ "$registry_type" == "app" ]; then
        export CONTAINER_ENGINE=docker
    fi
    generate_yaml
    if [ "$group" == "container_startup_test" ]; then
        avocado -V run --nrunner-max-parallel-tasks 1 $TONE_BM_RUN_DIR/image_ci/tests/smoke --mux-yaml $TONE_BM_RUN_DIR/hosts.yaml -t container
    elif [ "$group" == "application_container_func_test" ]; then
        if [ "$registry_type" == "app" ]; then
            avocado -V run --nrunner-max-parallel-tasks 1 $TONE_BM_RUN_DIR/image_ci/tests/smoke --mux-yaml $TONE_BM_RUN_DIR/hosts.yaml -t app_container
        else
            echo "====SKIP:$group"
        fi
    else
        echo "$group is not supported"
        return 1
    fi
    cp -rH $HOME/avocado/job-results/latest $TONE_CURRENT_RESULT_DIR
}

parse()
{
    $TONE_BM_SUITE_DIR/parse.py
}

generate_yaml()
{
    registry_addr=(`echo $REGISTRY_ADDR | tr '#' ' '`)
    c_engine=(`echo $CONTAINER_ENGINE | tr '/' ' '`)
cat > hosts.yaml << EOF
hosts: !mux
    localhost:
        host: localhost
registry: !mux
EOF
    for ((i = 0; i < ${#registry_addr[@]}; i++)); do
cat >> hosts.yaml << EOF
    registry$i:
        version: ${registry_addr[$i]}
EOF
    done

cat >> hosts.yaml << EOF
engines: !mux
EOF
    for ((i = 0; i < ${#c_engine[@]}; i++)); do
cat >> hosts.yaml << EOF
    engine$i:
        engine: ${c_engine[$i]}
EOF
    done
}