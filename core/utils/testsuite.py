#!/usr/bin/env python
# -*- coding: utf-8 -*-


class TESTSUITE(object):
    '''
        # parse test-suite and reformate it
        # The format is:
        # {
        #   test-suite: {
        #       index : int,
        #       scenaria : [
        #           {
        #               name: str,
        #               env: {},
        #           }
        #       ]
        #   }
        # }

    '''

    def __init__(self):
        self.suites = {}
        self.index = []

    def __str__(self):
        output = ""
        for suite in self.suites:
            output += suite + "\n"
            output += "\tindex: " + str(self.suites[suite]['index']) + "\n"
            output += "\t" + "\n\t".join(
                [str(x) for x in self.suites[suite]['scenaria']])
        return output

    def __repr__(self):
        return self.__str__()

    def __getitem__(self, ind):
        # pdb.set_trace()
        if isinstance(ind, str):
            if ind in self.suites:
                return self.suites[ind]
            else:
                return None
        if isinstance(ind, int) and ind < len(self.index):
            return self.suites[self.index[ind]]

    def __iter__(self):
        return iter(self.index)

    def add(self, suite, env, runtimes=1):
        if suite not in self.suites:
            self.suites[suite] = {
                'index': len(self.index),
                'scenaria': [],
            }
            self.index.append(suite)
        if 'testconf' in env:
            confname = env['testconf']
            del env['testconf']
        else:
            confname = "unknown"
        for k in [k for k, v in env.items() if v in (str(None))]:
            del env[k]
        self.suites[suite]['scenaria'].append({
            'scenaria': confname,
            'env': env,
            'runtimes': runtimes,
        })
