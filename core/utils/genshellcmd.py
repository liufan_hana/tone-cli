#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os


def real_path(path):
    realpath = ""
    if path[0] == '~':
        realpath = os.environ['HOME'] + path[1:]
        return os.path.abspath(realpath)
    return os.path.abspath(path)


def create_path(path):
    realpath = real_path(path)
    return "[ -d {0} ] || mkdir -p {0}\n".format(realpath)


def create_path_force(path):
    realpath = real_path(path)
    cmdfmt = (
        "[ -d {0} ] && rm -rf {0}\n"
    )
    cmdfmt += create_path(path)
    return cmdfmt.format(realpath)


def copy_file(src, des, permission=None):
    if src == des:
        return

    if os.path.exists(des):
        Warning("{} exists!".format(des))
    else:
        cmdfmt = "cp -af {0} {1}\n"
        if permission:
            cmdfmt += (
                "chmod -R {2} {1}\n"
            )
            return cmdfmt.format(src, des, permission)
        else:
            return cmdfmt.format(src, des)


def copy_all(src, des, permission=None):
    return copy_file(
        os.path.join(src, '*'),
        os.path.join(des, '*'),
        permission)


def copy_tree(src, des, permission=None):
    return copy_all(src, des, permission)


if __name__ == "__main__":
    print create_path("~/tone")
    print create_path_force("~/test")
