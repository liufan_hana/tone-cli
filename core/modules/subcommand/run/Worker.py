#!/usr/bin/env python
# -*- coding: utf-8 -*-

from module_interfaces import TESTMODULE
from utils.tests import TestCommands
# from utils.log import logger


class Worker(TESTMODULE):
    priority = 200

    def run(self, test_instance):
        if test_instance.config.subcommand == 'archive':
            return
        testcmd = TestCommands(test_instance.config)
        if test_instance.config.subcommand == 'fetch':
            testcmd.fetch()
        if test_instance.config.subcommand == 'install':
            testcmd.install()
        if test_instance.config.subcommand == 'uninstall':
            testcmd.uninstall()
