#!/usr/bin/env python
# -*- coding: utf-8 -*-

from utils.tests import TestEnvInit
from module_interfaces import TESTMODULE
# from utils.log import logger


class TestSetup(TESTMODULE):
    priority = 0

    def run(self, test_instance):
        setup = TestEnvInit(test_instance.config)
        setup.create_result_runtime_path()
        setup.set_env()
        setup.test_info()
